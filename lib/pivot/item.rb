module Pivot
  class Item
    attr_reader :name, :assignee, :points

    PROJECT_CODES = [
      EREC = "EREC".freeze,
      AZR = "AZR".freeze
    ].freeze

    def initialize(name:, assignee:, points:)
      @name = name
      @assignee = assignee
      @points = points
    end

    def +(item)
      points + item.points
    end

    def project_code
      @project_code ||= name.match(/\A([A-Z]+)-/)[1]
    end

    def valid?
      PROJECT_CODES.include?(project_code)
    end

    def assign(person)
      @assignee = person.email
    end
  end
end
